from django.test import TestCase, Client
from django.urls import resolve
from django.apps import apps
from appajax.views import *
from appajax.apps import *

# Create your tests here.
class story9UnitTest(TestCase):
    def test_story9_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_story9_url_is_notexist(self):
        response = Client().get('/notexist/')
        self.assertEqual(response.status_code, 404)

    def test_using_books_func(self):
        found = resolve('/')
        self.assertEqual(found.func, book_func)

    def test_apps(self):
        self.assertEqual(BooksConfig.name, 'appajax')
        self.assertEqual(apps.get_app_config('appajax').name, 'appajax')
