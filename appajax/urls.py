from django.urls import path
from . import views
urlpatterns = [
    path('', views.book_func, name='book_search'),
    path('jsonreq/<str:book>',views.jsonreq_func, name='jsonreq_func')
]